FROM python:3.12-slim

RUN apt update && apt install git vim -y && \
    apt-get clean && rm -rf /var/lib/apt/lists/* && \
    git clone https://gitlab.gnome.org/GNOME/releng.git /opt/releng

COPY requirements.txt /opt/releng
RUN pip install -r /opt/releng/requirements.txt

ENTRYPOINT ["/usr/bin/python3"]
